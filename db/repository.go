package db

import (
	. "bitbucket.org/iminsoft/auth-srv/model"
)

type (
	SessionRepository interface {
		Create(*Session)
		Delete(string)
		Read(string) (*Session, bool)
	}
)
