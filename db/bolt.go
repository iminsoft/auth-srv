package db

import (
	"encoding/json"
	"fmt"
	"log"

	. "bitbucket.org/iminsoft/auth-srv/model"
	"github.com/boltdb/bolt"
)

var (
	sessionsBucket = []byte("Sessions")
	instance       *bolt.DB
)

type (
	boltRepo struct {
		conn *bolt.DB
	}
)

func init() {
	var err error
	instance, err = bolt.Open("auth.db", 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
}

func (r *boltRepo) Create(s *Session) {
	r.conn.Batch(func(tx *bolt.Tx) error {
		b, err := tx.CreateBucketIfNotExists(sessionsBucket)
		if err != nil {
			return err
		}

		ou, err := json.Marshal(s)
		if err != nil {
			return err
		}

		err = b.Put([]byte(s.Id), ou)
		if err != nil {
			return err
		}

		return nil
	})
}

func (r *boltRepo) Delete(id string) {
	r.conn.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket(sessionsBucket)
		if b == nil {
			return fmt.Errorf("Bucket %q not found!", sessionsBucket)
		}

		err := b.Delete([]byte(id))
		if err != nil {
			return err
		}

		return nil
	})
}

func (r *boltRepo) Read(id string) (*Session, bool) {
	s := Session{}
	r.conn.View(func(tx *bolt.Tx) error {
		b := tx.Bucket(sessionsBucket)
		if b == nil {
			return fmt.Errorf("Bucket %q not found!", sessionsBucket)
		}

		v := b.Get([]byte(id))
		err := json.Unmarshal(v, &s)
		if err != nil {
			return err
		}

		return nil
	})

	return &s, s.Gid != ""
}

func NewRepository() SessionRepository {
	return &boltRepo{
		conn: instance,
	}
}
