package handler

import (
	"crypto/rand"
	"time"

	"bitbucket.org/iminsoft/auth-srv/db"
	. "bitbucket.org/iminsoft/auth-srv/model"

	"encoding/hex"
	"fmt"

	"github.com/micro/go-micro/server"
	ldap "github.com/sokool/goldap"
	"github.com/sokool/goldap/filter"
	"golang.org/x/net/context"
)

var (
	alphanum = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
)

type Auth struct {
	repo db.SessionRepository
}

func (e *Auth) Login(ctx context.Context, req *LoginRequest, res *LoginResponse) error {
	if req.Username == "" || req.Password == "" {
		return fmt.Errorf("Wrong username/password")
	}

	ad := ldap.New()
	el, has := ad.Search([]string{"objectGUID"}, map[string][]string{}).
		When(filter.AND(filter.Equal("sAMAccountName", req.Username))).
		FetchOne()

	if !has {
		return fmt.Errorf("Wrong username/password")
	}

	if !ad.Authenticate(req.Username, req.Password) {
		return fmt.Errorf("Wrong username/password")
	}

	session := &Session{
		Id:      random(32),
		Gid:     hex.EncodeToString(el.GetRawAttributeValue("objectGUID")),
		Created: time.Now().Unix(),
		Expires: time.Now().Add(time.Hour * 24 * 7).Unix(),
	}

	//SAVE SESSION
	e.repo.Create(session)

	res.Session = session

	return nil
}

func (e *Auth) Logout(ctx context.Context, req *LogoutRequest, res *LogoutResponse) error {
	e.repo.Delete(req.SessionId)

	return nil
}

func (e *Auth) Session(ctx context.Context, req *SessionRequest, res *SessionResponse) error {

	if s, is := e.repo.Read(req.SessionId); is {
		res.Session = s
	}

	return nil
}

func random(i int) string {
	bytes := make([]byte, i)
	for {
		rand.Read(bytes)
		for i, b := range bytes {
			bytes[i] = alphanum[b%byte(len(alphanum))]
		}
		return string(bytes)
	}
	return "ughwhy?!!!"
}

func Register(s server.Server) {
	s.Handle(s.NewHandler(&Auth{
		repo: db.NewRepository(),
	}))
}
