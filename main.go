package main

import (
	"log"

	_ "bitbucket.org/iminsoft/auth-srv/db"
	"bitbucket.org/iminsoft/auth-srv/handler"
	"github.com/micro/cli"
	micro "github.com/micro/go-micro"
	ldap "github.com/sokool/goldap"
)

func main() {
	service := micro.NewService(
		micro.Name("im.srv.auth"),
		micro.Flags(
			cli.StringFlag{
				Name:   "active_directory_url, a",
				EnvVar: "ACTIVE_DIRECTORY_URL",
				Usage:  "<username>:<password>@<repository>:<port>",
			},
		),
		micro.Action(func(c *cli.Context) {
			if len(c.String("a")) > 0 {
				ldap.Url = c.String("a")
			}
		}),
	)

	service.Init()
	handler.Register(service.Server())

	if err := service.Run(); err != nil {
		log.Fatal(err)
	}

}
