package model

type (
	LoginRequest struct {
		Username string `json:"username,omitempty" description:"pole usera"`
		Password string `json:"password,omitempty" description:"pole hasła"`
	}

	LoginResponse struct {
		Session *Session `json:"session,omitempty"`
	}

	LogoutRequest struct {
		SessionId string `json:"sessionId,omitempty"`
	}

	LogoutResponse struct {
	}

	SessionRequest struct {
		SessionId string `json:"sessionId,omitempty"`
	}

	SessionResponse struct {
		Session *Session `json:"session,omitempty"`
	}
)

func (*LoginRequest) ProtoMessage() {}
