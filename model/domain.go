package model

type Session struct {
	Id      string `json:"id,omitempty"`
	Gid     string `json:"gid,omitempty"`
	Created int64  `json:"created,omitempty"`
	Expires int64  `json:"expires,omitempty"`
}
